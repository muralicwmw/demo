<!DOCTYPE html>
<html>
<head>
<title>Magickwoods - Kitchen Visualizer</title>
<link rel="shortcut icon" type="image/png" href="http://localhost/lowes/3d/images/favicon.png"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<style>
	body{font-family: 'Roboto', sans-serif;}
	body p, div{font-family: 'Roboto', sans-serif;}	
	body select{font-family: 'Roboto', sans-serif; margin:5px 0px;}
	img{max-width:100%;}
	.design-container{position:relative;margin:10px 0px;}
	.design-img{position:absolute; left:0; right:0;}
	h4.h4-title {font-size: 17px;text-transform: uppercase;font-weight: 350;font-family: 'Roboto', sans-serif;text-align:center; color:#275C82;}
	.footer-section{padding:0px 0px 20px 0px;}
	.social-share a{padding: 5px;font-size: 25px;position: relative;top: 12px;display: inline-block;}
	.selected-link { margin-top: 10px;}
	.social-share span {position: relative; top: 10px;}
	.selected-link a {padding-left: 10px; display:inline-block; text-decoration:none;}
	.more-text {margin-top: 8px;}	
	a:hover{text-decoration:none;}
	a.more-link {padding-right: 5%; font-style:italic;}	
	a.more-link:after {
		font-family: FontAwesome;
		content: '\f08e';
		position: relative;
		bottom: 5px;
		left: 5px;
		font-size:8px;
		font-style:normal !important;
	}	
	.more-text a:after {
		font-family: FontAwesome;
		content: '\f08e';
		position: relative;
		bottom: 5px;
		left: 5px;
		font-size:8px;
	}	
</style>
</head>

<?php

if(isset($_GET['options'])) {
	list($door, $worktop, $handle, $floor) = explode("/", $_GET['options']);
}
$door_array = array(
	'Door - Altino_Alabaster',
	'Door - Altino_Black',
	'Door - Altino_Cashmere',
	'Door - Altino_Champagne',
	'Door - Altino_Dakar'
);
$worktop_array = array(
	'Worktop - Dekton_edora',
	'Worktop - Granite_mondaritz',
	'Worktop - Laminate_sonoma_oak',
	'Worktop - Quartz_asphalt',
	'Worktop - Solid_wood_beech'
);
$handle_array = array(
	'Handle - Bar_Standard',
	'Handle - Boss_Standard',
	'Handle - Bow_Standard',
	'Handle - Cup_Standard',
	'Handle - Drop_Standard'
);
$floor_array = array(
	'Floor - Tiles_1',
	'Floor - Tiles_2',
	'Floor - Tiles_3',
	'Floor - Tiles_4',
	'Floor - Tiles_5'
);
function form_selectbox_options($config, $img) {
	foreach($config as $value){
		$value_explode = explode(' - ',$value);
		//$t .= "<option value='http://localhost/lowes/3d/images/".$img."/".$value_explode[1].".png'>".$value."</option>";
		echo "<option value='http://localhost/lowes/3d/images/".$img."/".$value_explode[1].".png'>".$value."</option>";
	}
	//return $t;
}
?>

<body onload="javascript:update_selectedlink()";>
	<div class="container">
		<div class="row header-section">
			<div class="col-md-2" style="padding-top:10px;">
				<a href="/3d/">
					<img id="logo-image" src="http://localhost/lowes/3d/images/mw_logo.png" width="200px" alt="logo"/>
				</a>
				<h4 class="h4-title">Kitchen Visualizer</h4> 
			</div>
			<div class="col-md-10 text-right">
				<div class="more-text">
					<a href='#' id="selected-link" target="_blank">Share your visualization:</a> 
				</div>
				<div class="selected-link">
					<a href='http://localhost/lowes/3d/urls.php' class="more-link" target="_blank">More Unique Kitchens</a>
					<a target="_blank" href="#" id="facebook">
						<img src="http://localhost/lowes/3d/images/facebook.png" alt="facebook"/>
					</a>
					<a target="_blank" href="#" id="twitter">
						<img src="http://localhost/lowes/3d/images/twitter.png" alt="twitter"/>
					</a>
					<a target="_blank" href="#" id="google-plus">
						<img src="http://localhost/lowes/3d/images/google-plus.png" alt="google plus"/>
					</a>
					<a target="_blank" href="#" id="pinterest">
						<img src="http://localhost/lowes/3d/images/pinterest.png" alt="pinterest"/>
					</a>						
				</div>
			</div>			
		</div>
		<div class="row text-center">
			<div class="col-md-3">
				<select id="door" class="form-control selectpicker" name="door" onchange="kitchen_select()">
					<?php echo form_selectbox_options($door_array, 'door'); ?>
				</select>
			</div>
			<div class="col-md-3">
				<select id="worktop" class="form-control selectpicker" name="worktop" onchange="kitchen_select()">
					<?php echo form_selectbox_options($worktop_array, 'worktop'); ?>
				</select>		
			</div>
			<div class="col-md-3">
				<select id="handle" class="form-control selectpicker" name="handle" onchange="kitchen_select()">
					<?php echo form_selectbox_options($handle_array, 'handle'); ?>
				</select>
			</div>
			<div class="col-md-3">
				<select id="floor" class="form-control selectpicker" name="floor" onchange="kitchen_select()">
					<?php echo form_selectbox_options($floor_array, 'floor'); ?>
				</select>				
			</div>
		</div>		
	</div>
	<div class="container">
		<div class="design-container">
			<img id="wall-image" class="design-img" src="http://localhost/lowes/3d/images/Wall_Overlay.png" />
			<img id="door-image" class="design-img" src="http://localhost/lowes/3d/images/door/<?php $door_array_explode = explode(" - ", $door_array[0]);echo $door_array_explode[1]; ?>.png" />
			<img id="worktop-image" class="design-img" src="http://localhost/lowes/3d/images/worktop/<?php $worktop_array_explode = explode(" - ",$worktop_array[0]); echo  $worktop_array_explode[1]; ?>.png" />
			<img id="handle-image" class="design-img" src="http://localhost/lowes/3d/images/handle/<?php $handle_array_explode = explode(" - ",$handle_array[0]); echo  $handle_array_explode[1]; ?>.png" />
			<img id="floor-image" src="http://localhost/lowes/3d/images/floor/<?php $floor_array_explode = explode(" - ",$floor_array[0]); echo  $floor_array_explode[1]; ?>.png" />
		</div>
	</div>	
	<div class="container footer-section">
		<div class="row">
			<div class="col-md-12">
				<div class="text-center">&copy; Copyright 2018 Magick Woods</div>
			</div>
		</div>
	</div>
	
		<div class="row" style="display:none;">
			<img src="http://localhost/lowes/3d/images/door/Altino_Alabaster.png" alt="door"/>
			<img src="http://localhost/lowes/3d/images/door/Altino_Black.png" alt="door"/>
			<img src="http://localhost/lowes/3d/images/door/Altino_Cashmere.png" alt="door"/>
			<img src="http://localhost/lowes/3d/images/door/Altino_Champagne.png" alt="door"/>
			<img src="http://localhost/lowes/3d/images/door/Altino_Dakar.png" alt="door"/>
			<!--Flooring-->
			<img src="http://localhost/lowes/3d/images/floor/Tiles_1.png" alt="flooring"/>
			<img src="http://localhost/lowes/3d/images/floor/Tiles_2.png" alt="flooring"/>
			<img src="http://localhost/lowes/3d/images/floor/Tiles_3.png" alt="flooring"/>
			<img src="http://localhost/lowes/3d/images/floor/Tiles_4.png" alt="flooring"/>
			<img src="http://localhost/lowes/3d/images/floor/Tiles_5.png" alt="flooring"/>			
		</div>	
			
	<script>		
	var image_door_name_remove_exe = "<?php echo $door_array_explode[1]; ?>";
	var image_worktop_name_remove_exe = "<?php echo $worktop_array_explode[1]; ?>";
	var image_handle_name_remove_exe = "<?php echo $handle_array_explode[1]; ?>";
	var image_floor_name_remove_exe = "<?php echo $floor_array_explode[1]; ?>";

	//Door image
	function kitchen_select() {
		//doorimage
		var image_door = document.getElementById("door-image"),
		select_door = document.getElementsByTagName('select')[0];
		var image_door_name = select_door.value.replace(/^.*[\\\/]/, '');
		image_door_name_remove_exe = image_door_name.replace('.png', '');
		image_door.src = select_door.value;
		
		//worktop
		var image_worktop = document.getElementById("worktop-image"),
		select_worktop = document.getElementsByTagName('select')[1];
		var image_worktop_name = select_worktop.value.replace(/^.*[\\\/]/, '');
		image_worktop_name_remove_exe = image_worktop_name.replace('.png', '');
		image_worktop.src = select_worktop.value;
		
		//Handle
		var image_handle = document.getElementById("handle-image"),
		select_handle = document.getElementsByTagName('select')[2];
		var image_handle_name = select_handle.value.replace(/^.*[\\\/]/, '');
		image_handle_name_remove_exe = image_handle_name.replace('.png', '');
		image_handle.src = select_handle.value;
		
		//floor
		var image_floor = document.getElementById("floor-image"),
		select_floor = document.getElementsByTagName('select')[3];			
		var image_floor_name = select_floor.value.replace(/^.*[\\\/]/, '');
		image_floor_name_remove_exe = image_floor_name.replace('.png', '');
		image_floor.src = select_floor.value;		
		
		update_selectedlink();
	}

 	function update_selectedlink() {
		var link = image_door_name_remove_exe + "/" + image_worktop_name_remove_exe + "/" + image_handle_name_remove_exe + "/" + image_floor_name_remove_exe + "/";
		document.getElementById("selected-link").href = "http://localhost/lowes/3d/" + link;
		document.getElementById("facebook").href = "http://www.facebook.com/sharer/sharer.php?u=http://localhost/lowes/3d/" + link;	
		document.getElementById("twitter").href = "http://twitter.com/share?url=http://localhost/lowes/3d/" + link + "&text=Unique Kitchen Design";	
		document.getElementById("google-plus").href = "https://plus.google.com/share?url=http://localhost/lowes/3d/" + link;
		document.getElementById("pinterest").href = "http://pinterest.com/pin/create/button/?url=http://localhost/lowes/3d/" + link + "&description=Unique Kitchen Design";		
	} 

	</script>	
</body>
</html>